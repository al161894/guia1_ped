﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIA1_PED
{
    public partial class Ejemplo1 : Form
    {
        //constructor
        public Ejemplo1()
        {
            InitializeComponent();
            Is1 = Is2 = false;
        }

        //*************************************************metodos
        public void limpiar_pantalla()
        {
            pantalla.Text = "";
        }

        public double obtener_valor()//para transformar el string del txtbox a int
        {            
            double valor = Convert.ToDouble(pantalla.Text);
            limpiar_pantalla();
            return valor;                        
        }

        public void actualizar_pantalla(string texto)//para actualizar lo que se visualiza en el txtbox
        {
            pantalla.Text = pantalla.Text + texto;
        }

        public double operar(double operador1, String signo)
        {
            double Resultado = 0.0;
            switch (signo)
            {
                case "seno":
                    Resultado = Math.Sin(operador1);
                    break;

                case "coseno":
                    Resultado = Math.Cos(operador1);
                    break;

                case "tang":
                    Resultado = Math.Tan(operador1);
                    break;

                case "ln":
                    Resultado = Math.Log(operador1);
                    break;

                case "log":
                    Resultado = Math.Log10(operador1);
                    break;

                default:
                    break;
            }
            btnDot.Enabled = true;
            return Resultado;
        }

        public double operar(double operador1, double operador2, String signo)
        {
            double Resultado = 0.0;
            switch (signo)
            {
                case "+":
                    Resultado = operador1 + operador2;
                    break;
                case "-":
                    Resultado = operador1 - operador2;
                    break;
                case "*":
                    Resultado = operador1 * operador2;
                    break;
                case "/":
                    if (operador1 == 0)
                    {
                        MessageBox.Show("No se puede dividir entre 0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        limpiar_pantalla();
                        actualizar_pantalla("");
                    }
                    else
                    {
                        Resultado = operador1 / operador2;
                    }
                    break;
                case "^":
                    Resultado = Math.Pow(operador1, operador2);
                    break;

                default:
                    break;
            }
            btnDot.Enabled = true;
            return Resultado;
        }


        //*************************************************declaracion de variables
        public double Num1, Num2, Resultado;//Num1 y 2 para capturar los operandos a utilizar
        public bool Is1, Is2, Es_op;//controlan la operatividad de la calculadora
        int Operacion;//capturan la operacion a realizar

        //*************************************************botones numericos        
        private void btn0_Click(object sender, EventArgs e)
        {
            actualizar_pantalla("0");
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            actualizar_pantalla("1");
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            actualizar_pantalla("2");
        }        

        private void btn3_Click(object sender, EventArgs e)
        {
            actualizar_pantalla("3");
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            actualizar_pantalla("4");
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            actualizar_pantalla("5");
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            actualizar_pantalla("6");
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            actualizar_pantalla("7");
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            actualizar_pantalla("8");
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            actualizar_pantalla("9");
        }

        //*************************************************botones operaciones        
        private void btnSuma_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Is1)//false
                {
                    Num1 = obtener_valor();
                    Is1 = true; //actualizamos el valor de la variable control
                    Operacion = 0;//0 indica que la operacion es suma
                }
                else
                {
                    MessageBox.Show("Ingresa un número");
                }
                btnDot.Enabled = true;
            }
            catch
            {
                MessageBox.Show("Debes ingresar un número", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }

        private void btnResta_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Is1)//false
                {
                    Num1 = obtener_valor();
                    Is1 = true;
                    Operacion = 1;
                }
                btnDot.Enabled = true;
            }
            catch
            {
                MessageBox.Show("Debes ingresar un número", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }

        private void btnMulti_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Is1)//false
                {
                    Num1 = obtener_valor();
                    Is1 = true;
                    Operacion = 2;
                }
                btnDot.Enabled = true;
            }
            catch
            {
                MessageBox.Show("Debes ingresar un número", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }

        private void btnDiv_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Is1)//false
                {
                    Num1 = obtener_valor();
                    Is1 = true;
                    Operacion = 3;
                }
                btnDot.Enabled = true;
            }
            catch
            {
                MessageBox.Show("Debes ingresar un número", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }

        private void btnIgual_Click(object sender, EventArgs e)
        {
            try
            {
                if(Operacion == 0)//si es suma
                {
                    if (Is1)//true
                    {
                        Num2 = obtener_valor();//para obtener el segundo operando para la suma
                        actualizar_pantalla(operar(Num1, Num2, "+").ToString());
                        Is1 = false;
                    }
                    else
                    {
                        MessageBox.Show("Seleccione una operacion a realizar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }else if(Operacion == 1)//resta
                {
                    if (Is1)//true
                    {
                        Num2 = obtener_valor();
                        actualizar_pantalla(operar(Num1, Num2, "-").ToString());
                        Is1 = false;
                    }
                    else
                    {
                        MessageBox.Show("Seleccione una operacion a realizar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else if (Operacion == 2)//multiplicacion
                {
                    if (Is1)//true
                    {
                        Num2 = obtener_valor();
                        actualizar_pantalla(operar(Num1, Num2, "*").ToString());
                        Is1 = false;
                    }
                    else
                    {
                        MessageBox.Show("Seleccione una operacion a realizar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else if (Operacion == 3)//division
                {
                    if (Is1)//true
                    {
                        Num2 = obtener_valor();
                        actualizar_pantalla(operar(Num1, Num2, "/").ToString());
                        Is1 = false;
                    }
                    else
                    {
                        MessageBox.Show("Seleccione una operacion a realizar","Atención",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                    }
                }
                else if(Operacion == 4)//seno
                {
                    if (Is1)
                    {
                        actualizar_pantalla(operar(Num1, "seno").ToString());
                        Is1 = false;
                    }
                    else
                    {
                        MessageBox.Show("Selecciona la propiedad trigonométrica");
                    }
                }
                else if (Operacion == 5)//coseno
                {
                    if (Is1)
                    {
                        actualizar_pantalla(operar(Num1, "coseno").ToString());
                        Is1 = false;
                    }
                    else
                    {
                        MessageBox.Show("Selecciona la propiedad trigonométrica");
                    }
                }
                else if (Operacion == 6)//tang
                {
                    if (Is1)
                    {
                        actualizar_pantalla(operar(Num1, "tang").ToString());
                        Is1 = false;
                    }
                    else
                    {
                        MessageBox.Show("Selecciona la propiedad trigonométrica");
                    }
                }
                else if (Operacion == 7)//potencia
                {
                    if (Is1)//true
                    {
                        Num2 = obtener_valor();
                        actualizar_pantalla(operar(Num1, Num2, "^").ToString());
                        Is1 = false;
                    }
                    else
                    {
                        MessageBox.Show("Seleccione una operacion a realizar", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else if (Operacion == 8)//ln
                {
                    if (Is1)
                    {
                        actualizar_pantalla(operar(Num1, "ln").ToString());
                        Is1 = false;
                    }                    
                }
                else if (Operacion == 9)//logaritmo
                {
                    if (Is1)
                    {
                        actualizar_pantalla(operar(Num1, "log").ToString());
                        Is1 = false;
                    }
                }
            }
            catch
            {
                MessageBox.Show("Está operación requierda dos operandos","Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }                

        private void btnDot_Click(object sender, EventArgs e)
        {
            if (!Is1)
            {
                if ((pantalla.Text.Length > 0) && !(pantalla.Text.Contains(".")))
                {
                    actualizar_pantalla(".");
                    btnDot.Enabled = false;
                }
            }else if (Is1)
            {                
                if ((pantalla.Text.Length > 0) && !(pantalla.Text.Contains(".")))
                {
                    actualizar_pantalla(".");
                    btnDot.Enabled = false;
                }
            }
        }

        private void btnSen_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Is1)//false
                {
                    Num1 = obtener_valor();
                    Is1 = true;
                    Operacion = 4;//seno
                }
                btnDot.Enabled = true;
            }
            catch
            {
                MessageBox.Show("Debes ingresar un número","Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void btnCos_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Is1)//false
                {
                    Num1 = obtener_valor();
                    Is1 = true;
                    Operacion = 5;//coseno
                }
                btnDot.Enabled = true;
            }
            catch
            {
                MessageBox.Show("Debes ingresar un número", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }

        private void btnTan_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Is1)//false
                {
                    Num1 = obtener_valor();
                    Is1 = true;
                    Operacion = 6;//tangente
                }
                btnDot.Enabled = true;
            }
            catch
            {
                MessageBox.Show("Debes ingresar un número", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPotencia_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Is1)//false
                {
                    Num1 = obtener_valor();
                    Is1 = true;
                    Operacion = 7;
                }
                btnDot.Enabled = true;
            }
            catch
            {
                MessageBox.Show("Debes ingresar un número", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnLn_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Is1)//false
                {
                    Num1 = obtener_valor();
                    Is1 = true;
                    Operacion = 8;//ln
                }
                btnDot.Enabled = true;
            }
            catch
            {
                MessageBox.Show("Debes ingresar un número", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnLog_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Is1)//false
                {
                    Num1 = obtener_valor();
                    Is1 = true;
                    Operacion = 9;//log
                }
                btnDot.Enabled = true;
            }
            catch
            {
                MessageBox.Show("Debes ingresar un número", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnC_Click(object sender, EventArgs e)
        {
            limpiar_pantalla();
            btnDot.Enabled = true;
        }

        private void btnCE_Click(object sender, EventArgs e)
        {
            limpiar_pantalla();
            btnDot.Enabled = true;
        }

        private void pantalla_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))//obtiene el valor de la tecla presionada y verifica si es numero
            {
                e.Handled = false;//manejador de error desactivado
                //verifica que se pueda ingresar puntos y un solo punto
            }
            else
            {
                e.Handled = true;
                MessageBox.Show("Solo se admiten datos numéricos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void Ejemplo1_Load(object sender, EventArgs e)
        {
            ToolTip ToolPotencias = new ToolTip();
            ToolPotencias.SetToolTip(btnPotencia, "El primer numero es la base, y el segundo la potencia!");
        }

          


    }
}
